from rdflib import Graph
import pyshacl
import os

class NormEngine:
    def __init__(self):
        # TO DO: Load ontologies for FLINT, source, scenario (actually not essential for the norm engine to work right now)

        # Load domain ontology
        self.domain_ontology = self.parse_folder('project/ontologies')

        # Load interpretation and implementation of the facts
        self.interpretation = self.parse_folder('project/interpretations')
        self.implementation = self.parse_folder('project/implementations')
        self.scenario = None
        self.namespaces = None

    def new_graph(self):
        g = Graph()
        if self.namespaces:
            for prefix, namespace in self.namespaces.namespaces():
                g.bind(prefix, namespace)
        return g
  
    def parse_folder(self,folder):
        g = Graph()
        for file in os.listdir(folder):
            if file.endswith('.ttl'):
                g += Graph().parse(os.path.join(folder,file), format="turtle")
        return g

    def run_construct_query(self,query_file,input_graph,output_graph):
        with open(os.path.join('queries',query_file)) as f:
            triples = input_graph.query(f.read())
        for triple in triples:
            output_graph.add(triple)

    def run_pyshacl(self,data,shacl):
        run_again = True
        while run_again: # Workaround because iterate_rules=True does not always work as expected
            triples_before = len(data)
            result = pyshacl.validate(
                data,
                shacl_graph=shacl,
                data_graph_format="turtle",
                shacl_graph_format="turtle",
                advanced=True,
                iterate_rules=True,
                inplace=True,
                inference="owlrl",
                debug=False
            )
            run_again = len(data) > triples_before
        return result
  
    def select_scenario(self):
        scenarios = []
        for file in os.listdir('project/scenarios'):
            if file.endswith('.ttl'):
                scenarios.append(file)
        if not len(scenarios):
            raise Exception('No scenario files found')
        print("Available scenarios:")
        for id, scenario in enumerate(scenarios):
            print(id + 1, scenario)

        while True:
            id = input("Enter the number of the scenario you want to load and press ENTER: ")
            try:
                if scenarios[int(id) - 1]:
                    self.scenario = os.path.join('project/scenarios',scenarios[int(id) - 1])
                    break
            except:
                pass

    def run(self): # TO DO: this should be split up in several methods

        if not self.scenario or not os.path.isfile(self.scenario):
            self.select_scenario()

        self.scenario_graph = Graph().parse(self.scenario, format='turtle')
        
        self.scenario_with_ontology = self.scenario_graph + self.domain_ontology

        # Run SHACL on domain ontology and scenario (in case the domain ontology contains SHACL rules)
        self.run_pyshacl(self.scenario_with_ontology,self.domain_ontology)
        
        self.namespaces = self.interpretation + self.implementation + self.scenario_with_ontology

        # Run summary queries
        with open('queries/summary.rq') as f:
            for row in self.interpretation.query(f.read()):
                print()
                print("====== SUMMARY FOR PRECONDITION ======")
                print(f"Precondition: {row.plabel}")
                print(f"Act: {row.alabel}")
                print(f"Text fragment: {row.fragment}")
                result = self.scenario_with_ontology.query(row.query)
                if len(result):
                    print("--------------------------------------")
                    
                    for row in result:
                        for var in result.vars:
                            print(f"{var}: {row[var]}")
                else:
                    print('No results')
                print()

        
        # Prepare SHACL to check preconditions
        preconditions = self.new_graph()
        self.run_construct_query('precondition_shacl.rq', self.interpretation + self.implementation + self.scenario_with_ontology, preconditions)

        # Run precondition SHACL
        conforms, report_graph, report_text = self.run_pyshacl(self.scenario_with_ontology,preconditions)

        # Print validation results
        print(report_text)

        if not conforms:
            report_graph += self.scenario_with_ontology + preconditions + self.implementation + self.interpretation
            with open('queries/report.rq') as f:
                for row in report_graph.query(f.read()):
                    print("====== PRECONDITION NOT MET ======")
                    print(f"Precondition: {row.precondition}")
                    print(f"Label: {row.label}")
                    if row.source:
                        print(f"Source: {row.source}")
                    if row.fragment:
                        print(f"Text fragment: {row.fragment}")
                    print("==================================")

        # Prepare SHACL to derive postconditions
        postconditions = self.new_graph()
        self.run_construct_query('postcondition_shacl.rq', self.interpretation + self.implementation + self.scenario_with_ontology, postconditions)

        # Run postcondition SHACL
        self.run_pyshacl(self.scenario_with_ontology,postconditions)

        # Save result from applying postconditions
        self.scenario_with_ontology.serialize(destination='project/output/scenario.ttl', format='turtle')

        # Copy facts to new scenario (we create a new graph for readability, but this could also be done in the existing graph)
        new_scenario = self.new_graph()
        self.run_construct_query('new_scenario.rq', self.scenario_with_ontology, new_scenario)
        self.run_construct_query('creating_postconditions.rq', self.scenario_with_ontology + new_scenario, new_scenario)

        # Save new scenario graph
        new_scenario.serialize(destination='project/output/new_scenario.ttl',format='turtle')

        # Save SHACL graphs for debugging purposes
        preconditions.serialize(destination='project/output/precondition_shacl.ttl',format='turtle')
        postconditions.serialize(destination='project/output/postcondition_shacl.ttl',format='turtle')