# Calculemus Calculator

An implementation of normative reasoning using Semantic Web technology, in the context of the Calculemus protocol for normative coordination and the Flint language for modeling action-based normative systems.

Based on a collaboration between TNO and @fbakker.

To use the Calculator, one needs to [define a project](#specifying-a-project) and some project scenario. Some example projects are here:

1.
1.

## Running the Calculator

The Calculator runs in Python. It uses [pyshacl](https://github.com/RDFLib/pySHACL).

## Architecture

An implementation of Flint-based reasoning consists of several general knowledge artifacts, as well as some project-specific knowledge. The image below shows how different layers of knowledge are combined for the reasoner engine. Normative sources are interpreted using the Flint language. This specifies a normative state machine, where transitions are normative acts which are characterized by a collection of normative facts. Through a mapping function, observations in brute reality can be qualified as facts in institutional reality. Scenarios describe the taking of an action and the state of both layers of reality at that moment. The reasoner engine will then detect the performing of a normative act, if applicable. Based on the normative act's postconditions, the reasoner engine will determine a new state of the normative system.

![](architecture.png)

## Specifying a project

The Calculator is domain-independent. It consumes a set of project specifications, and outputs the results of running through any or all scenarios defined in that project. The file organization for a project reflects the reasoner architecture discussed above. A project repository should generally be structured as follows:

- `scenarios\...`: concrete cases for testing and development
- `sources\...`: relevant normative sources
- `interpretation.ttl`: an interpretation of the sources
- `brute-reality.ttl`: a domain ontology of brute reality
- `implementation.ttl`: mappings to relate concepts from the source interpretation to observations in brute reality, using SHACL. This is used for reasoning.
    - Recognizes if normative acts are applicable and allowed (preconditions met)
    - Realizes postconditions

## A description of the reasoning process

1. Load state of brute and institutional reality from scenario
1. Run SHACL validation on state
1. Each Act is a SHACL shape that checks if the state conforms to it
    - Are the preconditions for the Act met?
    - Is there a set of observations in brute reality that can qualify as the Act's components (action, actor, ...)?
1. If the state conforms, the corresponding `sh:construct` and `sh:TripleRule` are triggered
    - Creating postconditions:
    - Terminating postconditions:
